<?php

namespace App\Infrastructure\Http\Rest\Controller;


use App\Application\Service\UserService;
use Doctrine\ORM\EntityNotFoundException;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use FOS\RestBundle\View\View;

class UserController extends FOSRestController
{
    /**
     * @var UserService
     */
    private $userService;

    /**
     * UserController constructor.
     * @param UserService $userService
     */
    public function __construct(UserService $userService)
    {
        $this->userService = $userService;
    }

    /**
     * Creates an User resource
     * @Rest\Post("/users")
     * @param Request $request
     * @return View
     */
    public function postUser(Request $request): View
    {
        $phone = $this->requestGetPhone($request);
        $user = $this->userService->addUser($request->get('name'), $phone);

        // In case our POST was a success we need to return a 201 HTTP CREATED response
        return View::create($user, Response::HTTP_CREATED);
    }

    /**
     * Retrieves an User resource
     * @Rest\Get("/users/search/{userName}")
     * @param string $userName
     * @return View
     */
    public function getUsersByName(string $userName): View
    {
        $users = $this->userService->getUsersByName($userName);

        // In case our GET was a success we need to return a 200 HTTP OK response with the request object
        return View::create($users, Response::HTTP_OK);
    }

    /**
     * Retrieves an User resource
     * @Rest\Get("/users")
     * @return View
     */
    public function getAllUsers(): View
    {
        $users = $this->userService->getAllUsers();

        // In case our GET was a success we need to return a 200 HTTP OK response with the request object
        return View::create($users, Response::HTTP_OK);
    }

    /**
     * Replaces User resource
     * @Rest\Put("/users/{userId}")
     * @param int $userId
     * @param Request $request
     * @return View
     * @throws EntityNotFoundException
     */
    public function putUser(int $userId, Request $request): View
    {
        $phone = $this->requestGetPhone($request);
        $user = $this->userService->updateUser($userId, $request->get('name'), $phone);

        // In case our PUT was a success we need to return a 200 HTTP OK response with the object as a result of PUT
        return View::create($user, Response::HTTP_OK);
    }

    /**
     * Removes the User resource
     * @Rest\Delete("/users/{userId}")
     * @param int $userId
     * @return View
     * @throws EntityNotFoundException
     */
    public function deleteUser(int $userId): View
    {
        $this->userService->deleteUser($userId);

        // In case our DELETE was a success we need to return a 204 HTTP NO CONTENT response. The object is deleted.
        return View::create([], Response::HTTP_NO_CONTENT);
    }

    /**
     * @param Request $request
     * @return int
     */
    private function requestGetPhone(Request $request) : int
    {
        $phone = $request->get('phone');
        if(!is_integer($phone)) {
            throw new \InvalidArgumentException('Phone number requires numeric value.');
        }

        return $phone;
    }
}