<?php

namespace App\Application\Service;


use App\Domain\Model\User\User;
use App\Domain\Model\User\UserRepositoryInterface;
use Doctrine\ORM\EntityNotFoundException;

final class UserService
{
    /**
     * @var UserRepositoryInterface
     */
    private $userRepository;

    /**
     * UserService constructor.
     * @param UserRepositoryInterface $userRepository
     */
    public function __construct(UserRepositoryInterface $userRepository){
        $this->userRepository = $userRepository;
    }

    /**
     * @param int $userId
     * @return User|null
     */
    public function getUser(int $userId): ?User
    {
        return $this->userRepository->findById($userId);
    }

    /**
     * @param string $userName
     * @return array|null
     */
    public function getUsersByName(string $userName): ?array
    {
        return $this->userRepository->findByName($userName);
    }

    /**
     * @return array|null
     */
    public function getAllUsers(): ?array
    {
        return $this->userRepository->findAll();
    }

    /**
     * @param string $name
     * @param int $phone
     * @return User
     */
    public function addUser(string $name, int $phone): User
    {
        $user = new User();
        $user->setName($name);
        $user->setPhone($phone);
        $this->userRepository->save($user);
        return $user;
    }

    /**
     * @param int $userId
     * @param string $name
     * @param int $phone
     * @return User|null
     * @throws EntityNotFoundException
     */
    public function updateUser(int $userId, string $name, int $phone): ?User
    {
        $user = $this->userRepository->findById($userId);
        if (!$user) {
            throw new EntityNotFoundException('User with id '.$userId.' does not exist!');
        }
        $user->setName($name);
        $user->setPhone($phone);
        $this->userRepository->save($user);
        return $user;
    }

    /**
     * @param int $userId
     * @throws EntityNotFoundException
     */
    public function deleteUser(int $userId): void
    {
        $user = $this->userRepository->findById($userId);
        if (!$user) {
            throw new EntityNotFoundException('User with id '.$userId.' does not exist!');
        }
        $this->userRepository->delete($user);
    }

}